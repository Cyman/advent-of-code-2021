import copy

file = open("input/d12.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    connections = []
    for line in lines:
        con_from, con_to = line.strip().split('-')
        connections.append((con_from, con_to))
    return connections

def find_connected_nodes(connections, current_node):
    connected_nodes = []
    for connection in connections:
        con_from, con_to = connection
        if con_from == current_node:
            connected_nodes.append(con_to)
        if con_to == current_node:
            connected_nodes.append(con_from)
    return connected_nodes

def build_paths_to_end(connections, current_node, current_path, paths, p2):
    connected_nodes = find_connected_nodes(connections, current_node)

    for connected_node in connected_nodes:
        if connected_node == 'end':
            path = copy.deepcopy(current_path)
            path.append('end')
            paths.append(path)
            continue

        if connected_node == 'start':
            continue

        if connected_node.islower():
            if current_path.__contains__(connected_node):
                if not p2 or path_contains_small_cave_twice(current_path):
                    continue

        path = copy.deepcopy(current_path)
        path.append(connected_node)
        build_paths_to_end(connections, connected_node, path, paths, p2)

def path_contains_small_cave_twice(path):
    for node in path:
        if node != 'start' and node.islower():
            count = 0
            for searched_node in path:
                if searched_node == node:
                    count += 1
            if count == 2:
                return True
    return False

def part_1(connections):
    paths = []
    build_paths_to_end(connections, 'start', ['start'], paths, False)
    print(paths.__len__())

def part_2(connections):
    paths = []
    build_paths_to_end(connections, 'start', ['start'], paths, True)
    print(paths.__len__())

connections = parse_input(lines)
part_1(connections)
part_2(connections)
