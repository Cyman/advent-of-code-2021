file = open("input/d8.txt")
lines = file.readlines()
file.close()

class Entry:
    def __init__(self, unique_signal_patterns, output_values):
        self.unique_signal_patterns = unique_signal_patterns
        self.output_values = output_values

def parse_input(lines):
    entries = []
    for line in lines:
        (str_sig_patterns, str_output_values) = line.strip().split(" | ")
        entries.append(
            Entry(
                str_sig_patterns.split(' '),
                str_output_values.split(' ')
            )
        )
    return entries

def find_element_by_length(items, length):
    for item in items:
        if item.__len__() == length:
            return item
    return None

def find_elements_by_length(items, length):
    elements = []
    for item in items:
        if item.__len__() == length:
            elements.append(item)
    return elements

def chars_from_string(str):
    chars = []
    for char in str:
        chars.append(char)
    return chars

def contains_all_chars(str, chars):
    contains_all = True
    str_chars = chars_from_string(str)
    for char in chars:
        if not str_chars.__contains__(char):
            contains_all = False
    return contains_all

def get_keys(combinations):
    keys = {}
    candidates = {}
    segments = ["top", "top_left", "top_right", "middle", "bottom_left", "bottom_right", "bottom"]
    for segment in segments:
        candidates[segment] = []
    display_1 = find_element_by_length(combinations, 2)
    display_4 = find_element_by_length(combinations, 4)
    display_7 = find_element_by_length(combinations, 3)
    display_1_chars = chars_from_string(display_1)
    display_4_chars = chars_from_string(display_4)
    display_7_chars = chars_from_string(display_7)
    
    # top segment is easy to figure out, we just have to check which segment 
    # lights up in 7 but doesn't in 1, rest is either top_right or bottom_right
    if display_1 is not None and display_7 is not None:
        for char in display_7_chars:
            if not display_1_chars.__contains__(char):
                keys["top"] = char
            else:
                candidates["top_right"].append(char)
                candidates["bottom_right"].append(char)

    # save chars which light up in 4 but don't light up in 1, so middle and top_left
    for char in display_4_chars:
        if char != keys["top"] and not display_1_chars.__contains__(char):
            candidates["middle"].append(char)
            candidates["top_left"].append(char)

    length_5_elements = find_elements_by_length(combinations, 5)

    # we can find 5 by length and by checking which segments light up from leftovers from 4
    display_5 = None
    display_5_chars = []
    for element in length_5_elements:
        if contains_all_chars(element, candidates["middle"]):
            display_5 = element
            display_5_chars = chars_from_string(display_5)
    assert(display_5 is not None)

    # we know which two segments are top_right and bottom_right (from 1), so the one not lighting up
    # in 5 is top_right, while the other is bottom_right
    for candidate in candidates["top_right"]:
        if display_5_chars.__contains__(candidate):
            keys['bottom_right'] = candidate
        else:
            keys['top_right'] = candidate

    # we know the key of bottom_right, so from length 5 elements (2,3,5) if the element
    # is not 5 and it doesn't have bottom_right lit up, it's 2, otherwise it's 3
    display_2 = None
    display_2_chars = []
    display_3 = None
    display_3_chars = []
    for element in length_5_elements:
        element_chars = chars_from_string(element)
        if element != display_5 and element_chars.__contains__(keys["bottom_right"]):
            display_3 = element
            display_3_chars = element_chars
        elif element != display_5 and not element_chars.__contains__(keys["bottom_right"]):
            display_2 = element
            display_2_chars = element_chars
    assert(display_2 is not None)
    assert(display_3 is not None)

    # 2 and 3 are different only by one segment, so we can easily find bottom_left
    for char in display_2_chars:
        if not display_3_chars.__contains__(char):
            keys["bottom_left"] = char

    # 3 and 5 are different only by one segment, so we can easily find top_left
    for char in display_5_chars:
        if not display_3_chars.__contains__(char) and char != keys["top_right"]:
            keys["top_left"] = char

    # only bottom and middle remain unsolved, middle lights up in diff between 1 and 4
    solved = list(keys.values())
    for char in ["a", "b", "c", "d", "e", "f", "g"]:
        if not solved.__contains__(char) and candidates['top_left'].__contains__(char):
            keys["middle"] = char
        elif not solved.__contains__(char) and not candidates['top_left'].__contains__(char):
            keys["bottom"] = char

    return keys

def get_digit_from_str_and_keys(str, keys):
    if str.__len__() == 2:
        return 1
    if str.__len__() == 4:
        return 4
    if str.__len__() == 3:
        return 7
    if str.__len__() == 7:
        return 8
    str_chars = chars_from_string(str)
    if str.__len__() == 6:
        if not str_chars.__contains__(keys["middle"]):
            return 0
        if not str_chars.__contains__(keys["top_right"]):
            return 6
        if not str_chars.__contains__(keys["bottom_left"]):
            return 9
    if str.__len__() == 5:
        if str_chars.__contains__(keys["bottom_left"]):
            return 2
        if str_chars.__contains__(keys["top_right"]):
            return 3
        return 5

def part_1(entries):
    counter = 0
    for entry in entries:
        for output_value in entry.output_values:
            if output_value.__len__() in [2,3,4,7]:
                counter += 1
    print(counter)

def part_2(entries):
    sum = 0

    for entry in entries:
        keys = get_keys(entry.unique_signal_patterns)
        digits = ""
        for output_value in entry.output_values:
            digits += str(get_digit_from_str_and_keys(output_value, keys))
        sum += int(digits)

    print(sum)

entries = parse_input(lines)
part_1(entries)
part_2(entries)
