from collections import Counter

file = open("input/d14.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    template = lines[0].strip()
    rules = {}
    for line in lines[2:]:
        a, b = line.strip().split(" -> ")
        rules[a] = b
    return (template, rules)

def solve(template, rules, steps):
    # count pairs in initial template
    pair_counter = Counter()
    for id, char in enumerate(template):
        if id != len(template) - 1:
            pair_counter[template[id] + template[id + 1]] += 1

    # count pairs after every step
    for _ in range(steps):
        new_pair_counter = Counter()
        for pair, count in list(pair_counter.items()):
            new_pair_counter[pair[0] + rules[pair]] += count
            new_pair_counter[rules[pair] + pair[1]] += count
        pair_counter = new_pair_counter

    # count letters in pairs
    letter_counter = Counter()
    for pair, count in list(pair_counter.items()):
        letter_counter[pair[0]] += count
    letter_counter[template[-1]] += 1

    most_common = max(letter_counter.values())
    least_common = min(letter_counter.values())
    print(most_common - least_common)

def part_1(template, rules):
    solve(template, rules, 10)

def part_2(template, rules):
    solve(template, rules, 40)

template, rules = parse_input(lines)
part_1(template, rules)
part_2(template, rules)
