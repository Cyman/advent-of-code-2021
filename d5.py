file = open("input/d5.txt")
lines = file.readlines()
file.close()

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class LineSegment:
    def __init__(self, a, b):
        self.a = a
        self.b = b

def parse_input(lines):
    line_segments = []
    for line in lines:
        str_point_a,str_point_b = line.split(" -> ")
        a_x,a_y = str_point_a.split(',')
        b_x,b_y = str_point_b.split(',')
        line_segments.append(LineSegment(Point(int(a_x), int(a_y)), Point(int(b_x), int(b_y))))
    return line_segments

def insert_vertical_line(map, line_segment):
    assert(line_segment.a.x == line_segment.b.x)
    x = line_segment.a.x

    if line_segment.a.y < line_segment.b.y:
        point_from = line_segment.a
        point_to = line_segment.b
    else:
        point_from = line_segment.b
        point_to = line_segment.a

    for n in range(point_from.y, point_to.y + 1):
        map[x][n] += 1

    return map

def insert_horizontal_line(map, line_segment):
    assert(line_segment.a.y == line_segment.b.y)
    y = line_segment.a.y

    if line_segment.a.x < line_segment.b.x:
        point_from = line_segment.a
        point_to = line_segment.b
    else:
        point_from = line_segment.b
        point_to = line_segment.a

    for n in range(point_from.x, point_to.x + 1):
        map[n][y] += 1

    return map

def insert_diagonal_line(map, line_segment):
    if line_segment.a.x < line_segment.b.x:
        point_from = line_segment.a
        point_to = line_segment.b
    else:
        point_from = line_segment.b
        point_to = line_segment.a

    y_rising = point_from.y < point_to.y
    x = point_from.x
    y = point_from.y

    while x <= point_to.x:
        map[x][y] += 1
        x += 1
        if y_rising:
            y += 1
        else:
            y -= 1

    return map

def part_1():
    line_segments = parse_input(lines)
    line_segments = list(filter(
        lambda line_segment : line_segment.a.x == line_segment.b.x or line_segment.a.y == line_segment.b.y,
        line_segments
    ))
    map = [ [0] * 1000 for _ in range(1000)]

    for line_segment in line_segments:
        if line_segment.a.x == line_segment.b.x:
            # horizontal
            map = insert_vertical_line(map, line_segment)
        else:
            # vertical
            map = insert_horizontal_line(map, line_segment)

    count = 0
    for column in map:
        count += list(filter(lambda x : x >= 2, column)).__len__()
    print(count)

def part_2():
    line_segments = parse_input(lines)
    map = [ [0] * 1000 for _ in range(1000)]

    for line_segment in line_segments:
        if line_segment.a.x == line_segment.b.x:
            # horizontal
            map = insert_vertical_line(map, line_segment)
        elif line_segment.a.y == line_segment.b.y:
            # vertical
            map = insert_horizontal_line(map, line_segment)
        else:
            # diagonal
            map = insert_diagonal_line(map, line_segment)

    count = 0
    for column in map:
        count += list(filter(lambda x : x >= 2, column)).__len__()
    print(count)

part_1()
part_2()
