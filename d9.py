file = open("input/d9.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    map = []
    for line in lines:
        map.append([int(char) for char in line.strip()])
    return map

def part_1(map):
    sum = 0
    for y, row in enumerate(map):
        for x, v in enumerate(row):
            if (x == row.__len__() - 1 or v < map[y][x + 1]) and \
                (x == 0 or v < map[y][x - 1]) and \
                (y == map.__len__() - 1 or v < map[y + 1][x]) and \
                (y == 0 or v < map[y - 1][x]):
                sum += (v + 1)
    print(sum)

def list_unique(arr):
    return list(set(arr))

def find_basin_elements(map, x, y, visited):
    result = []
    result.append((x, y))
    visited.append((x, y))
    # go left
    if not visited.__contains__((x - 1, y)) and x != 0 and map[y][x - 1] != 9:
        result += find_basin_elements(map, x - 1, y, visited)
    if not visited.__contains__((x + 1, y)) and x != map[0].__len__() - 1 and map[y][x + 1] != 9:
        result += find_basin_elements(map, x + 1, y, visited)
    if not visited.__contains__((x, y - 1)) and y != 0 and map[y - 1][x] != 9:
        result += find_basin_elements(map, x, y - 1, visited)
    if not visited.__contains__((x, y + 1)) and y != map.__len__() - 1 and map[y + 1][x] != 9:
        result += find_basin_elements(map, x, y + 1, visited)
    return result

def part_2(map):
    basins = []
    for y, row in enumerate(map):
        for x, v in enumerate(row):
            if v != 9:
                new_basin = True
                for basin in basins:
                    if basin.__contains__((x, y)):
                        new_basin = False
                if new_basin:
                    basin_elements = list_unique(find_basin_elements(map, x, y, []))
                    basins.append(basin_elements)
    lengths = [basin.__len__() for basin in basins]
    lengths.sort()
    lengths = list(reversed(lengths))
    print(lengths[0] * lengths[1] * lengths[2])

map = parse_input(lines)
part_1(map)
part_2(map)
