file = open("input/d2.txt")
lines = file.readlines()
file.close()

def part_1():
    x = 0
    y = 0
    for line in lines:
        instruction,value = line.split(' ')
        value = int(value)
        if instruction == "up":
            y -= value
        elif instruction == "down":
            y += value
        elif instruction == "forward":
            x += value
    print(x * y)

def part_2():
    x = 0
    y = 0
    aim = 0
    for line in lines:
        instruction,value = line.split(' ')
        value = int(value)
        if instruction == "up":
            aim -= value
        elif instruction == "down":
            aim += value
        elif instruction == "forward":
            x += value
            y += aim * value
    print(x * y)

part_1()
part_2()
