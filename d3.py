file = open("input/d3.txt")
lines = file.readlines()
file.close()

length = lines[0].__len__() - 1

def part_1():
    counter = {}
    counter[0] = {}
    counter[1] = {}

    for i in range(length):
        counter[0][i] = 0
        counter[1][i] = 0

    for line in lines:
        for c_id,character in enumerate(line):
            if character == "0":
                counter[0][c_id] += 1
            if character == "1":
                counter[1][c_id] += 1

    gamma = ""
    episolon = ""

    for i in range(length):
        if counter[0][i] > counter[1][i]:
            gamma += str(0)
            episolon += str(1)
        else:
            gamma += str(1)
            episolon += str(0)

    print(int(gamma, 2) * int(episolon, 2))

def part_2():
    gamma_candidates = lines
    epsilon_candidates = lines
    counter = {}

    for i in range(length):
        counter[0] = 0
        counter[1] = 0

        for line in gamma_candidates:
            if line[i] == "0":
                counter[0] += 1
            else: 
                counter[1] += 1

        if counter[0] > counter[1]:
            if gamma_candidates.__len__() != 1:
                gamma_candidates = list(filter(lambda x : x[i] == "0", gamma_candidates))
        else:
            if gamma_candidates.__len__() != 1:
                gamma_candidates = list(filter(lambda x : x[i] == "1", gamma_candidates))

    for i in range(length):
        counter[0] = 0
        counter[1] = 0

        for line in epsilon_candidates:
            if line[i] == "0":
                counter[0] += 1
            else: 
                counter[1] += 1

        if counter[1] < counter[0]:
            if epsilon_candidates.__len__() != 1:
                epsilon_candidates = list(filter(lambda x : x[i] == "1", epsilon_candidates))
        else:
            if epsilon_candidates.__len__() != 1:
                epsilon_candidates = list(filter(lambda x : x[i] == "0", epsilon_candidates))

    print(int(gamma_candidates[0], 2) * int(epsilon_candidates[0], 2))

part_1()
part_2()
