import copy

file = open("input/d11.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    for y, row in enumerate(lines):
        lines[y] = [int(v.strip()) for v in row.strip()]
    return lines

def flash(map, x, y, step_flashes, flash_count):
    for dy in (-1, 0, 1):
        for dx in (-1, 0, 1):
            # bound check
            try:
                v = map[y + dy][x + dx]
            except:
                continue

            if y + dy < 0 or x + dx < 0:
                continue

            step_flashes, flash_count = increment(map, x + dx, y + dy, step_flashes, flash_count)
    return (step_flashes, flash_count)

def increment(map, x, y, step_flashes, flash_count):
    map[y][x] += 1
    if map[y][x] > 9 and (x, y) not in step_flashes:
        step_flashes.append((x, y))
        flash_count += 1
        step_flashes, flash_count = flash(map, x, y, step_flashes, flash_count)
    return (step_flashes, flash_count)

def part_1(map):
    flash_count = 0
    for _ in range(100):
        step_flashes = []
        for y, row in enumerate(map):
            for x, v in enumerate(row):
                step_flashes, flash_count = increment(map, x, y, step_flashes, flash_count)
        for x, y in step_flashes:
            map[y][x] = 0
    print(flash_count)

def part_2(map):
    flash_count = 0
    step_count = 0
    while True:
        step_flashes = []
        for y, row in enumerate(map):
            for x, v in enumerate(row):
                step_flashes, flash_count = increment(map, x, y, step_flashes, flash_count)
        for x, y in step_flashes:
            map[y][x] = 0
        step_count += 1
        if step_flashes.__len__() == 100:
            print(step_count)
            return

map = parse_input(lines)
part_1(copy.deepcopy(map))
part_2(copy.deepcopy(map))
