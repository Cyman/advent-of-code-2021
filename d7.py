file = open("input/d7.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    numbers = lines.pop(0).split(',')
    numbers = [int(i) for i in numbers]
    return numbers

def part_1(numbers):
    min_v = min(numbers)
    max_v = max(numbers)
    fuel_costs = []
    for candidate in range(min_v, max_v):
        fuel_sum = 0
        for n in numbers:
            fuel_sum += abs(n - candidate)
        fuel_costs.append(fuel_sum)
    print(min(fuel_costs))

def part_2(numbers):
    min_v = min(numbers)
    max_v = max(numbers)
    fuel_costs = []
    for candidate in range(min_v, max_v):
        fuel_sum = 0
        for n in numbers:
            distance = abs(n - candidate)
            # https://researchmaniacs.com/Calculator/SumOfIntegers/Sum-of-integers-from-1-to-100.html
            fuel_sum += int(distance * (distance + 1) / 2)
        fuel_costs.append(fuel_sum)
    print(min(fuel_costs))

numbers = parse_input(lines)
part_1(numbers)
part_2(numbers)
